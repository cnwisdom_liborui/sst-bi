package com.syhdsoft.bi;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.net.NetworkRequest;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.ThreadUtils;
import com.blankj.utilcode.util.Utils;
//import com.sst.sysst.sdklib.ForeAndBack;
import com.sst.sysst.sdklib.InitSstSdk;

import java.util.List;


public class MainActivity extends AppCompatActivity implements  Utils.OnAppStatusChangedListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView ceshi = (TextView) this.findViewById(R.id.ceshi);
        ceshi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            InitSstSdk.reportParms(getApplicationContext(),"sst01l6exy9sdy1gosa",0,"","","",0,"");
                        }
                    }).start();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }


    @Override
    public void onForeground(Activity activity) {
//        ForeAndBack.foreGround();
    }

    @Override
    public void onBackground(Activity activity) {
        ThreadUtils.runOnUiThreadDelayed(new Runnable() {
            @Override
            public void run() {
                List<Activity> activities = ActivityUtils.getActivityList();
                if (activities.size() == 0) {
                    return;
                }
                if (!AppUtils.isAppForeground()) {
                    Toast.makeText(activity, "盛事通已切换到后台运行", Toast.LENGTH_SHORT).show();
                }
            }
        }, 500);
    }
}