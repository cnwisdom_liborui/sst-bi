package com.syhdsoft.bi;

import android.app.Activity;
import android.app.Application;
import android.content.res.Configuration;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.Utils;


public class Apl extends Application implements NetworkUtils.OnNetworkStatusChangedListener, Utils.OnAppStatusChangedListener {
    
    public String getAbc() {
        return abc;
    }

    public void setAbc(String abc) {
        this.abc = abc;
    }

    String abc;

    /*q
    Aplication创建时被调用，可以在该方法里进行一些初始化操作
     */
    @Override
    public void onCreate() {
        super.onCreate();
        AppUtils.registerAppStatusChangedListener(this);
    }

    /*
    系统配置发生变更 的时候被调用 如：屏幕方向更改 、系统语言更改
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /*
    系统内存吃紧时被调用，用于释放内存
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onForeground(Activity activity) {

    }

    @Override
    public void onBackground(Activity activity) {

    }

    @Override
    public void onDisconnected() {

    }

    @Override
    public void onConnected(NetworkUtils.NetworkType networkType) {

    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}