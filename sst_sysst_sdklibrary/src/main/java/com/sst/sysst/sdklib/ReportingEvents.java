package com.sst.sysst.sdklib;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.DeviceUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.TimeUtils;
import com.sst.sysst.sdklib.tools.HttpURLConn;
import com.sst.sysst.sdklib.tools.Propety;

import org.json.JSONObject;

public class ReportingEvents {
    //初始化函数访问网络服务
    public static String initServer(String appKey,Context context){
        try{
            String certificationService=HttpURLConn.checkCertification(context,appKey,"/api/uc/sstbi/certificationService/checkCertification");
            return certificationService;
        }catch (Exception e){
            e.printStackTrace();
        }
        return "";
    }
    public static String report(Context context,String appKey,String params,ReporterCallBack reporterCallBack){
        SharedPreferences sp = context.getApplicationContext().getSharedPreferences("sst_sy_sdk", Context.MODE_PRIVATE);
        Boolean sst_flag=sp.getBoolean("sst_flag",false);
        if(sst_flag){
            new Thread(() -> {
                String reportData=HttpURLConn.insertData(context,appKey,params);
                reporterCallBack.submitReportSucess(reportData);
            }).start();
            return "上报成功";
        }else{
            return "上报失败";
        }
    }
}
