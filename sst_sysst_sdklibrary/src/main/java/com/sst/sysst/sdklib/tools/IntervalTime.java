package com.sst.sysst.sdklib.tools;

import com.blankj.utilcode.constant.TimeConstants;
import com.blankj.utilcode.util.TimeUtils;

public class IntervalTime {
    public static long startTime(){
        long startTime = TimeUtils.getNowMills();
        return startTime;
    }
    public static int timeSpan(long intervalTime){
        long finishTime = TimeUtils.getNowMills();
        long betweenTime=TimeUtils.getTimeSpan(finishTime,intervalTime, TimeConstants.SEC);
        int betweenTimeInt=Integer.parseInt(String.valueOf(betweenTime));
        return betweenTimeInt;
    }
}
