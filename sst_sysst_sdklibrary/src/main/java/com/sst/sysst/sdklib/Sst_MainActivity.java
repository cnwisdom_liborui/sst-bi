package com.sst.sysst.sdklib;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.Utils;

//public class Sst_MainActivity extends AppCompatActivity implements NetworkUtils.OnNetworkStatusChangedListener, Utils.OnAppStatusChangedListener  {


public class Sst_MainActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sst_main);

        InitSstSdk.reportParms(getApplicationContext(),"sst01l6exy9sdy1gosa",0,"","","",0,"");


    }
    public static void initSstSdk(Context context, String  appId) {
        Intent intent = new Intent();
        intent.putExtra("appId", appId);
        intent.setClass(context, Sst_MainActivity.class);
        context.startActivity(intent);
    }
}