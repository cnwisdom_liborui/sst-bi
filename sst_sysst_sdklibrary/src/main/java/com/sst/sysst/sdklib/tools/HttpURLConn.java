package com.sst.sysst.sdklib.tools;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;

public class HttpURLConn {
    //把一个inputStream 转换成一个String
    public static String inputStreamToString(InputStream is,String encrypt) throws Exception{
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        int len = -1;
        byte[] buffer = new byte[1024];
        while((len=is.read(buffer))!=-1){
            baos.write(buffer, 0, len);
        }
        is.close();
        String content = new String(baos.toByteArray(),encrypt);
        return content;
    }

    public static String backData(String mUrl,String encrypt){
        String result ="";
        try{
            URL url = new URL(mUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5000);
            if (conn.getResponseCode() == 200) {
                // 获取返回的数据
                InputStream is = conn.getInputStream();
                result = inputStreamToString(is,encrypt);
                is.close();
            } else {
                Log.w("sdkResult", "请求失败");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return result;
    }
    public static String checkCertification(Context context,String appid,String mUrl) {
        try {
            String urls=Propety.domain+mUrl;
            URL url = new URL(urls);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            // 设置请求方式
            connection.setRequestMethod("POST");
            // 设置是否向HttpURLConnection输出
            connection.setDoOutput(true);
            // 设置是否从httpUrlConnection读入
            connection.setDoInput(true);
            // 设置是否使用缓存
            connection.setUseCaches(false);
            //设置参数类型是json格式
            connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            connection.connect();
            String params = "{\"appid\":\"" + appid + "\"}";
            Log.w("insertValue",params);
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
            writer.write(params);
            writer.close();
            int responseCode = connection.getResponseCode();
            String result = "";
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //定义 BufferedReader输入流来读取URL的响应
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    result += line;
                }
            }
            Log.w("insert-result",result);
            return result;
        }catch(Exception e){
            e.printStackTrace();
        }
        return "aaaa";
    }
    public static String insertData(Context context,String appid,String param){
        try {
            String urls=Propety.domain+"/api/uc/sstbi/biDataAcquisitionService/insertData";
            URL url = new URL(urls);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            // 设置请求方式
            connection.setRequestMethod("POST");
            // 设置是否向HttpURLConnection输出
            connection.setDoOutput(true);
            // 设置是否从httpUrlConnection读入
            connection.setDoInput(true);
            // 设置是否使用缓存
            connection.setUseCaches(false);
            //设置参数类型是json格式
            connection.setRequestProperty("Content-Type", "application/json;charset=utf-8");
            connection.connect();
            String params ="{\"appid\":\"" + appid + "\",\"dataList\":["+param+"]}";
            Log.w("insertValue",params);
            //加密
            String encryptData=AesUtils.aesEncryptString(params,AesUtils.M_KEY);
            Log.w("insertValueEn",encryptData);
            String lastData ="{\"data\":\"" + encryptData + "\"}";
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream(), "UTF-8"));
            writer.write(lastData);
            writer.close();
            int responseCode = connection.getResponseCode();
            String result = "";
            if (responseCode == HttpURLConnection.HTTP_OK) {
                //定义 BufferedReader输入流来读取URL的响应
                BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;
                while ((line = in.readLine()) != null) {
                    result += line;
                }
            }
            Log.w("insert-result",result);
            return result;
        }catch(Exception e){
            e.printStackTrace();
        }
        return "aaaa";
    }
}
