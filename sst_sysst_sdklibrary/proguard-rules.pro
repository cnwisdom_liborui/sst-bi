# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
-keepattributes SourceFile,LineNumberTable# 抛出异常时保留代码行号
-keep class com.sst.sysst.sdklib.InitSstSdk {*;}
-keep class com.sst.sysst.sdklib.ReportingEvents {*;}
-keepclasseswithmembernames class * {
    native <methods>;
}
-keep class com.iget.datareporter.**{*;}

#-keep class com.my.fxkj.sdk.ad.fxkjad.SplashAd_Fxkj_F {*;}
